const List = require("../index").List;
const Item = require("../index").Item;
const Cable = require("../index").Cable;
const CableInList = require("../index").CableInList;

module.exports.create = (req, res) => {
  List.create(req.body)
    .then(item => {
      res.status(201).json(item);
    })
    .catch((err) => {
      console.error(err);
      res.send(500);
    });
};

module.exports.getList = (req, res) => {
  List.findByPk(req.params.id, { include: [{ model: Item, as: 'items' }, { model: CableInList, include: [Cable], as: 'cables' }] })
    .then((item) => {
      res.json(item);
    })
    .catch((err) => {
      console.error(err);
      res.send(500);
    })
}

module.exports.delete = (req, res) => {
  List.destroy({ where: { id: req.params.id } })
    .then(() => {
      res.send(204);
    })
    .catch((err) => {
      console.error(err);
      res.send(500);
    })
};

module.exports.findAll = (req, res) => {
  List.findAll({ include: [{ model: Item, as: "items" }] })
    .then((items) => {
      res.json(items);
    })
    .catch((err) => {
      console.error(err);
      res.send(500);
    })
};

module.exports.update = (req, res) => {
  List.findByPk(req.params.id, { include: [{ all: true }] })
    .then((list) => {
      // console.log();
      let items = list.dataValues.items
      req.body.items.forEach(item => {
        let alreadyInList = false;
        items.forEach(existingItem => {
          if (item.id == existingItem.id) {
            alreadyInList = true;
          }
        });
        if (!alreadyInList) {
          items.push(item.id);
        }
      });
      delete req.body.items;
      list.setItems(items);
      list.update(req.body)
        .then((list) => {
          res.json(list);
        })
    })
    .catch((err) => {
      console.error(err);
      res.send(500);
    })
};

module.exports.deleteFromList = (req, res) => {
  List.findByPk(req.params.id, { include: [{ all: true }] })
    .then((list) => {
      let currentItems = list.dataValues.items;
      for (let index = 0; index < currentItems.length; index++) {
        const item = currentItems[index].dataValues;
        if (item.id == req.body.item.id){
          currentItems.splice(index, 1);
          break;
        }
      }
      list.setItems(currentItems)
        .then(() => {
          List.findByPk(req.params.id)
          .then((list) => {
            res.json(list);
          })
        })
    })
    .catch((err) => {
      res.send(500);
      console.error(err);
    })
}

