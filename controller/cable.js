const Cable = require("../index").Cable;
const List = require("../index").List;
const CableInList = require("../index").CableInList;

module.exports.create = (req, res) => {
  Cable.create(req.body)
  .then(item => {
    res.status(201).json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  });
};

module.exports.getCable = (req, res) => {
  Cable.findByPk(req.params.id)
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
}

module.exports.delete = (req, res) => {
  Cable.destroy({where: {id: req.params.id}})
  .then(() => {
    res.send(204);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.findByQr = (req, res) => {
  Cable.findOne({where: {qrcode: req.params.qrcode}})
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.findAll = (req, res) => {
  Cable.findAll()
  .then((items) => {
    res.json(items);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.update = (req, res) => {
  console.log(req.body);
  console.log(req.params.id);
  Cable.update(req.body, {where: {id: req.params.id}})
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.linkCable = (req, res) => {
  Cable.findByPk(req.body.cable.id).then((cable) => {
    List.findByPk(req.body.list.id).then((list) => {
      console.log("Req: " + req.body.amount);
      CableInList.create({amount: parseInt(req.body.amount)})
      .then((cableLink) => {
        cableLink.setCable(cable)
        .then((cableLink) => {
          cableLink.setList(list)
          .then(async (link) => {
            res.json(link);
            updateCableAmount(req);
          })
        })
      })
    })
  })
  .catch((err) => {
    console.error(err);
  })
}

function updateCableAmount(req, cable){
  CableInList.findAll({where: {cableId: req.body.cable.id}})
            .then((links) => {
              console.log("length: " + links.length);
              let amountOutside = 0;
              for(let i = 0; i < links.length; i++){
                amountOutside = amountOutside + parseInt(links[i].amount);
              }
              console.log(amountOutside);
              Cable.findByPk(req.body.cable.id).then((cable) => {
                cable.update({amount: cable.amountTotal - amountOutside});
              })
            })
}

module.exports.cableBack = (req, res) => {
  console.log(req.body);
  CableInList.create({amount: parseInt(req.body.amount) * -1, back: true, cableId: req.body.cable.id})
  .then((link) => {
    res.json(link);
    updateCableAmount(req);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
}

