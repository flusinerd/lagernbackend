const Item = require("../index").Item;

module.exports.create = (req, res) => {
  Item.create(req.body)
  .then(item => {
    res.status(201).json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  });
};

module.exports.getItem = (req, res) => {
  Item.findByPk(req.params.id)
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
}

module.exports.delete = (req, res) => {
  Item.destroy({where: {id: req.params.id}})
  .then(() => {
    res.send(204);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.findByQr = (req, res) => {
  Item.findOne({where: {qrcode: req.params.qrcode}})
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.findAll = (req, res) => {
  Item.findAll()
  .then((items) => {
    res.json(items);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

module.exports.update = (req, res) => {
  Item.update(req.body, {where: {id: req.params.id}})
  .then((item) => {
    res.json(item);
  })
  .catch((err) => {
    console.error(err);
    res.send(500);
  })
};

