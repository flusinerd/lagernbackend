const Sequelize = require("sequelize");
const sequelize = new Sequelize('eventbrigadeLager', 'eventbrigadeLager', 'Pw4eventbrigade#123', {
  host: 'nw-system.de',
  dialect: 'mysql',
  logging: false,
});

const bodyParser = require("body-parser");

const Express = require("express");
const app = Express();
const cors = require('cors')



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

const Item = sequelize.define('item', {
  name: {
    type: Sequelize.STRING,
  },
  qrcode: {
    type: Sequelize.STRING,
  },
  comment: {
    type: Sequelize.STRING,
  },
  outside: {
    type: Sequelize.BOOLEAN,
  },
  isChest: {
    type: Sequelize.BOOLEAN,
  }
});

const List = sequelize.define('list', {
  back: {
    type: Sequelize.BOOLEAN,
  },
  name: {
    type: Sequelize.STRING,
  },
});

const Cable = sequelize.define('cable', {
  name: {
    type: Sequelize.STRING,
  },
  qrcode: {
    type: Sequelize.STRING,
  },
  amount: {
    type: Sequelize.INTEGER,
  },
  amountTotal: {
    type: Sequelize.INTEGER,
  }
});

const CableInList = sequelize.define('cableInList', {
  amount: {
    type: Sequelize.INTEGER,
  },
  back: {
    type: Sequelize.BOOLEAN,
  }
})

List.belongsToMany(Item, { as: 'items', through: 'ItemList' });
Item.belongsToMany(List, { as: 'lists', through: 'ItemList' });
// Cable.belongsTo(Item, { as: 'chest' });
Cable.hasMany(CableInList);
CableInList.belongsTo(Cable);
CableInList.belongsTo(List);
List.hasMany(CableInList, { as: 'cables'});

module.exports =
  {
    Cable,
    Item,
    List,
    CableInList,
  }


const itemController = require("./controller/item");
const cableController = require("./controller/cable");
const listController = require("./controller/list");

//Route Definitions
//Items
app.route("/items")
  .post(itemController.create)
  .get(itemController.findAll);

app.route("/items/qrcode/:qrcode")
  .get(itemController.findByQr);

app.route("/items/:id")
  .delete(itemController.delete)
  .put(itemController.update)
  .get(itemController.getItem);

//Cables
app.route("/cables")
  .post(cableController.create)
  .get(cableController.findAll);

app.route("/cables/qrcode/:qrcode")
  .get(cableController.findByQr);

app.route("/cables/:id")
  .delete(cableController.delete)
  .put(cableController.update)
  .get(cableController.getCable);

app.route("/cables/link")
  .post(cableController.linkCable);

app.post('/cables/link/back', cableController.cableBack);

//Listen
app.route("/lists")
  .post(listController.create)
  .get(listController.findAll);

app.route("/lists/:id")
  .delete(listController.delete)
  .put(listController.update)
  .get(listController.getList);

app.post("/lists/:id/remove", listController.deleteFromList)

sequelize.sync();

app.listen(4000, () => {
  console.log("App listening on port", 4000);
})